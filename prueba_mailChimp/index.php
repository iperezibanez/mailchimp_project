<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="row text-center">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <span class="titulo textoPrincipalGris">TRS RAGA RACING 2019</span>
        <!-- Inicio mostrar la foto diferente de la 2019 RR 125cc -->
        <div class="row justify-content-center mt-3">
            <div class="col-md-8 text-center">
                <img src="http://www.trsmotorcycles.com/archivos/galeria/originales/c5e21c_123.jpg" class="img-fluid">
            </div>
        </div>
        <span class="textoPrincipalGris">TRS RAGA RACING 2019 125cc</span>
        <!-- Fin mostrar la foto diferente de la 2019 RR 125cc -->

        <hr class="lineaPrincipal">

        <div class="col-md-12 text-center" style="font-size: 12px">Las imágenes que se muestran en este sitio corresponden a motocicletas para uso en competición solo en circuitos cerrados.</div>
        <br>
        <span class="texto_cuerpo textoNormal2 text-left"><p>Llega al mercado la tercera generación y evolución del modelo mas R de TRS, la nueva RAGA RACING 2019, que estará en los Distribuidores de la marca desde este mismo mes de noviembre 2018!!!</p>
            <p>La nueva RR 2019, que se comercializará al igual que su predecesora en sus cuatro cilindradas, 125, 250, 280 y 300, continúa siendo el modelo bandera y mas vendido de TRS y este año, incorpora una larga lista de evoluciones y mejoras técnicas que sin lugar a duda permitirán seguir optimizando su comportamiento…</p></span>
    </div>
    <div class="col-md-2"></div>
    <div class="col-md-12"><button type="button" onclick="envio_campana()">Enviar noticia</button></div>
    <div class="col-md-12"><div id="mensaje"></button></div>
</div>
<div id="mensaje"></div>

</body>
</html>

<script>
    function envio_campana() {
        var xhttp = new XMLHttpRequest();
        var titulo = document.getElementsByClassName("titulo")[0].innerHTML;
        var ruta_imagen = document.images[0].src;
        var cuerpo = document.getElementsByClassName("texto_cuerpo")[0].innerHTML;

        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("mensaje").innerHTML = this.responseText;
            }
        };
        xhttp.open("POST", "gestion_campana.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("titulo=" + titulo + "&ruta_imagenes=" + ruta_imagen + "&cuerpo=" + cuerpo);
    }
</script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>